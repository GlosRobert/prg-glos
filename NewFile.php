<form method="post" action="vyhodnotit.php">
    <label>Jak se značí modulus?</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot1" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">%</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot1" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">//</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot1" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">:</label>
    </div>

    <!-- 2 -->

    <label>Jak se v Javascriptu vytváří funkce</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot2" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">def jmenoFunkce():</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot2" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">function jmenoFunkce(){}</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot2" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">fun jmenofunkce{}()</label>
    </div>

    <!-- 3 -->

    <label>Kolik má člověk prstů na rukou?</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot3" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">10</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot3" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">20</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot3" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">13</label>
    </div>

    <!-- 4 -->

    <label>O které kryptoměně Vojta pořád mluví?</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot4" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">Ethereum</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot4" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">Dogecoin</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot4" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">Bitcoin</label>
    </div>

    <!-- 5 -->
    <label>O čem povídá Patrikova stránka?</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot5" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">Klasicismus</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot5" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">Gotika</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot5" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">Renesance</label>
    </div>

    <!-- 6 -->

    <label>Co je SVG logo na Zdeňkově stránce?</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot6" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">Zdeňkovy iniciály</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot6" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">Nápis Robert je retard</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot6" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">Neudělal ho</label>
    </div>

    <!-- 7 -->

    <label>Jak se v Pythonu vypisuje do konzole</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot7" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">print()</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot7" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">console.log()</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot7" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">printf()</label>
    </div>

    <!-- 8 -->

    <label>Jak umocníme čísla ve většině programovacích jazyků</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot8" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">^</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot8" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">$</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot8" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">**</label>
    </div>

    <!-- 9 -->

    <label>Jak deklarujeme proměnnou v Javascriptu?</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot9" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">int cislo(1)</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot9" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">var a;</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot9" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">createVariable(b=5)</label>
    </div>

    <!-- 10 -->

    <label>Jak získáme výsledek po celočíselném dělení v Pythonu?</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot10" id="inlineRadio1" value="1">
        <label class="form-check-label" for="inlineRadio1">//</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot10" id="inlineRadio2" value="2">
        <label class="form-check-label" for="inlineRadio2">%</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ot10" id="inlineRadio2" value="3">
        <label class="form-check-label" for="inlineRadio3">zdeněk(5/4)</label>
    </div>


    <button type="submit" value="vyhodnotit">KRYPL</button>

</form>
